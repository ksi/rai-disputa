import {
  MantineColorScheme,
  SegmentedControl,
  useMantineColorScheme,
} from '@mantine/core';
import { IconMoon, IconSun, IconSunMoon } from '@tabler/icons-react';

export function ColorSchemeToggle() {
  const { setColorScheme } = useMantineColorScheme();

  return (
    <SegmentedControl
      data={[
        {
          value: 'light',
          label: <IconSun size={18} />,
        },
        {
          value: 'auto',
          label: <IconSunMoon size={18} />,
        },
        {
          value: 'dark',
          label: <IconMoon size={18} />,
        },
      ]}
      onChange={(selected) => setColorScheme(selected as MantineColorScheme)}
    />
  );
}
