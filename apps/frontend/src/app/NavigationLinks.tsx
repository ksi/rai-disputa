import { UnstyledButton } from '@mantine/core';
import { Link } from 'react-router-dom';
import classes from './Layout.module.css';

export const NavigationLinks = () => (
  <>
    <UnstyledButton className={classes.control} component={Link} to="/">
      Accueil
    </UnstyledButton>
    <UnstyledButton className={classes.control} component={Link} to="/map">
      Carte
    </UnstyledButton>
  </>
);
